function [ u, t ] = PRBS( U, P_p, T_a )
% gibt ein PRBS-Signal mit der �berabtastung P_p >= 1 zur�ck
%
% Parameter:
% U   ... Amplitude
% P_p ... �berabtastung
% T_a ... Abtastzeit
%
% R�ckgabewerte:
% u   ... mittelwertfreier Signalvektor
% t   ... Zeitvektor (Zeitpunkte zu denen die Werte von u geh�ren)

O_p = 10;                       % Ordnung des PRBS
p = [zeros(9,1); 1];            % initialisieren der ersten O_p Werte
n = 2^O_p-1;                    % Anzahl zu plottender Punkte

% f�llen der restlichen n*P_p Werte
for k=O_p+1:n
    p(k) = mod(p(k-7) + p(k-10), 2);
end

u = 2* U * p;

% �berabtastung: jeder Wert wird P_p mal aufgetragen
u = reshape(repmat(u,1,P_p)',size(u,1)*P_p,[]);


% Korrektur f�r mittelwertfreies Signal
U0 = mean(u);
u = u- U0;

% Zeitvektor (Zeitpunkte zu denen die Werte von u geh�ren)
t = ((0:n*P_p-1).*T_a)';

end

