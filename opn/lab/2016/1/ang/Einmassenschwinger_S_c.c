/********************************************************************************
 *                                                                              *
 *  Einmassenschwinger                      					                *
 *                                                                              *
 *  --------------------------------------------------------------------------- *
 *                                                                              *
 *  file name: Einmassenschwinger_S_c.c              				            *
 *                                                                              *
 *	notes:                                                                      *
 *																				*
 *                                                                              *
 *  inputs:     U1(0)= F         Kraft [N]                                      *
 *				U2(0)= m		 Masse [N]                			            *
 *              U3(0)= d         D�mpfung [Ns/m]                                *
 *              U4(0)= c         Federsteifigkeit [N/m]                         *
 *                                                                              *
 *  states:     x[0]=s			 Position [m]                                   *
 *              x[1]=w			 Geschwindigketeit [m/s]  						*
 *                                                                              *
 *  outputs:    y1[0]=s 		 Position [m]                                   *
 *              y1[1]=w          Geschwindigkeit [m/s]  	                    *
 *              y1[2]=a          Beschleunigung [m/s^2]  	                    *
 *                                                                              *
 *  parameters: p[0]=s0          AB Position [m]                                *
 *              p[1]=w0          AB Geschwindigketeit [m/s]                     *
 *																				*
 *																				*
 *	sample time: Kontinuierlich													*
 *																				*
 *																				*
 *                                                                              *
 * ---------------------------------------------------------------------------  *
 *                                                                              *
 *  author:   Tobias Gl�ck                                                      *
 *  date:     11.09.2008                                                        *
 *                                                                              *
 *                                                                              *
 ********************************************************************************/

#define S_FUNCTION_NAME Einmassenschwinger_S_c
#define S_FUNCTION_LEVEL 2


#include "simstruc.h"
#include <math.h>

#ifdef MATLAB_MEX_FILE
#include "mex.h"
#endif


/********************************************************************************
 * Macros                                                                       *
 ********************************************************************************/
#define U1(element) (*uPtrs1[element])             /* simplified access to input */
#define U2(element) (*uPtrs2[element])             /* simplified access to input */
#define U3(element) (*uPtrs3[element])             /* simplified access to input */
#define U4(element) (*uPtrs4[element])             /* simplified access to input */

	                                         /* simplified access to parameters */
#define s0  		RWork[0]
#define w0 			RWork[1]

/********************************************************************************
 * Global variables and constants                                               *
 ********************************************************************************/

#define ppi 3.141592653589793	//pi


/********************************************************************************
 * Definition of globally used functions                                        *
 ********************************************************************************/


/********************************************************************************
 *                                                                              *
 * C-Code S-Function Callback Methods                                           *
 *                                                                              *
 ********************************************************************************/


/********************************************************************************
 * mdlCheckParameters (not supported by RTW)                                    *
 ********************************************************************************/


/********************************************************************************
 * mdlInitializeSizes															*
 ********************************************************************************/

static void mdlInitializeSizes(SimStruct *S)
{
	int_T i;                                                   /* loop variable */


	ssSetNumSFcnParams(S, 2);		         /* number of dialog box parameters */
#ifdef MATLAB_MEX_FILE
	if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {}
	  else return;           /* Simulink will report a parameter mismatch error */
#endif

	for (i=0; i<ssGetSFcnParamsCount(S); i++)       /* parameter tuning al-		*/
		ssSetSFcnParamTunable(S, i, 0);             /* lowed during simulation  */
	
	ssSetNumContStates(S, 2);		             /* number of continuous states */
    ssSetNumDiscStates(S, 0);		               /* number of discrete states */

    if(!ssSetNumInputPorts( S, 4))	return;            /* number of input ports */
	ssSetInputPortWidth(S, 0, 1);           /* number of inputs at input port 0 */
    ssSetInputPortDirectFeedThrough(S, 0, 1);       /* direct input feedthrough */
	ssSetInputPortWidth(S, 1, 1);           /* number of inputs at input port 1 */
    ssSetInputPortDirectFeedThrough(S, 1, 1);       /* direct input feedthrough */
	ssSetInputPortWidth(S, 2, 1);           /* number of inputs at input port 2 */
    ssSetInputPortDirectFeedThrough(S, 2, 1);       /* direct input feedthrough */    
    ssSetInputPortWidth(S, 3, 1);           /* number of inputs at input port 3 */
    ssSetInputPortDirectFeedThrough(S, 3, 1);       /* direct input feedthrough */   

    if(!ssSetNumOutputPorts(S, 1))	return;           /* number of output ports */
    ssSetOutputPortWidth(   S, 0, 3);     /* number of outputs at output port 0 */   

    ssSetNumSampleTimes(  S, 1);                      /* number of sample times */
    ssSetNumRWork(        S, 2);              /* number of real    work vectors */
    ssSetNumIWork(        S, 0);              /* number of integer work vectors */
    ssSetNumPWork(        S, 0);              /* number of pointer work vectors */

	ssSetNumModes(        S, 0);           /* number of modes to switch between */
	ssSetNumNonsampledZCs(S, 0);        /* number of non-sampled zero-crossings */
}


/********************************************************************************
 * mdlInitializeSampleTimes														*
 ********************************************************************************/

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}


/********************************************************************************
 * mdlStart																		*
 ********************************************************************************/

#define MDL_START 
#if defined (MDL_START)
static void mdlStart (SimStruct *S)
{   real_T  *RWork           = ssGetRWork(S);

    s0  		    = mxGetPr(ssGetSFcnParam(S, 0))[0];
	w0 			    = mxGetPr(ssGetSFcnParam(S, 1))[0];
 
}
#endif   /* MDL_START */


/********************************************************************************
 * mdlInitializeConditions														*
 ********************************************************************************/
#define MDL_INITIALIZE_CONDITIONS
static void mdlInitializeConditions(SimStruct *S)
{
	real_T *x0      = ssGetContStates(S);
	real_T  *RWork           = ssGetRWork(S);    
    
	x0[0]           = s0;
	x0[1]			= w0;

}
/********************************************************************************
 * mdlOutputs																	*
 ********************************************************************************/

static void mdlOutputs(SimStruct *S, int_T tid)
{
	real_T            *y1     = ssGetOutputPortRealSignal(S,0);  
    InputRealPtrsType uPtrs1  = ssGetInputPortRealSignalPtrs(S,0);
	InputRealPtrsType uPtrs2  = ssGetInputPortRealSignalPtrs(S,1);    
	InputRealPtrsType uPtrs3  = ssGetInputPortRealSignalPtrs(S,2);     
    InputRealPtrsType uPtrs4  = ssGetInputPortRealSignalPtrs(S,3);  
    real_T            *x      = ssGetContStates(S);    
	real_T            *RWork  = ssGetRWork(S);
 
    real_T  s,w;
    real_T  F,m,d,c;

    /*Definition der Eing�nge und Zust�nde*/
    F=U1(0);
    m=U2(0);
    d=U3(0);
    c=U4(0);
    s=x[0];
    w=x[1];

	y1[0]=s;
	y1[1]=w;
    y1[2]=1/m*(-d*w-c*s+F); 

}
/********************************************************************************
 * mdlDerivatives																*
 ********************************************************************************/

#define MDL_DERIVATIVES
#if defined (MDL_DERIVATIVES)
static void mdlDerivatives(SimStruct *S)
{   
    real_T *dx   = ssGetdX(S);
	real_T *x    = ssGetContStates(S);                              
	InputRealPtrsType uPtrs1  = ssGetInputPortRealSignalPtrs(S,0);
	InputRealPtrsType uPtrs2  = ssGetInputPortRealSignalPtrs(S,1);    
	InputRealPtrsType uPtrs3  = ssGetInputPortRealSignalPtrs(S,2);     
    InputRealPtrsType uPtrs4  = ssGetInputPortRealSignalPtrs(S,3);     
	real_T  *RWork            = ssGetRWork(S);

    real_T  s,w;
    real_T  F,m,d,c;

    /*Definition der Eing�nge und Zust�nde*/
    F=U1(0);
    m=U2(0);
    d=U3(0);
    c=U4(0);

    s=x[0];
    w=x[1];

    /* Differentialgleichungen*/
    dx[0]=w;
    dx[1]=1/m*(-d*w-c*s+F); 
}
#endif   /* MDL_DERIVATIVES */
/********************************************************************************
 * mdlUpDate														     		*
 ********************************************************************************/
#undef MDL_UPDATE  /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
static void mdlUpdate(SimStruct *S, int_T tid)
  { 
  }
#endif /* MDL_UPDATE */

/********************************************************************************
 * mdlZeroCrossings																*
 ********************************************************************************/

/*
 * Zero crossing signals (zcSignals) give the numeric solver a hint to poten-
 * tially reduce its step size when approaching to a zero crossing.
 */

#undef MDL_ZERO_CROSSINGS
#if defined (MDL_ZERO_CROSSINGS)
static void mdlZeroCrossings(SimStruct *S)
{
}
#endif   /* MDL_ZERO_CROSSINGS */


/********************************************************************************
 * mdlTerminate																	*
 ********************************************************************************/

static void mdlTerminate(SimStruct *S)
{
}


/********************************************************************************
 * Compiler dependent settings                                                  *
 ********************************************************************************/

#ifdef	MATLAB_MEX_FILE 
#include "simulink.c"   
#else
#include "cg_sfun.h"    
#endif


/********************************************************************************
 * End of C-Code S-Function														*
 ********************************************************************************/
