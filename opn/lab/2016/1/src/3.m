%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.3: ETFE
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;

% Figures ein = 1
fig = 1;

% Parameter für Chirp- und PRBS-Signal:
N = 4096;
ws = 2*pi;    % [rad/s]
we = 10*2*pi; % [rad/s]
U = 10;
Ta = 25e-3;   % [s]

% Chirp Signal:
[ u_chirp, t_chirp ] = Chirp( U, ws, we, N, Ta );

% PRBS-Signal für Pp = 4:
[ u_prbs4, t_prbs4 ] = PRBS( U, 4, Ta );

% PRBS-Signal für Pp = 1:
[ u_prbs1, t_prbs1 ] = PRBS( U, 1, Ta );

% Darstellung der Zeitverläufe
if fig
figure(1)
plot(t_chirp, u_chirp);
title('Zeitverlauf Chirp-Signal');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;

figure(2)
plot(t_prbs4, u_prbs4);
title('Zeitverlauf PRBS-Signal mit Pp = 4');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;

figure(3)
plot(t_prbs1, u_prbs1);
title('Zeitverlauf PRBS-Signal mit Pp = 1');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;
end

% Vergleich des Amplitudenspektrums
% FFT normiert auf Signallänge
N_chirp = length(u_chirp);
N_prbs4 = length(u_prbs4);
N_prbs1 = length(u_prbs1);

u_chirp_fft = fft(u_chirp,N_chirp);
u_prbs4_fft = fft(u_prbs4,N_prbs4);
u_prbs1_fft = fft(u_prbs1,N_prbs1);

% Amplitude in dB und Phase in ° der Signale
u_chirp_ampl = 20*log10(abs(u_chirp_fft));
u_chirp_phase = unwrap(angle(u_chirp_fft))*(180/pi);
u_prbs4_ampl = 20*log10(abs(u_prbs4_fft));
u_prbs4_phase = unwrap(angle(u_prbs4_fft))*(180/pi);
u_prbs1_ampl = 20*log10(abs(u_prbs1_fft));
u_prbs1_phase = unwrap(angle(u_prbs1_fft))*(180/pi);

% Minimale Frequenz
dw_chirp = 2*pi/(N_chirp*Ta);
dw_prbs4 = 2*pi/(N_prbs4*Ta);
dw_prbs1 = 2*pi/(N_prbs1*Ta);

% Maximale Frequenz
w_max = pi/Ta;

% physikalisch sinnvoller Bereich
w_chirp = 0:dw_chirp:w_max-dw_chirp;
w_prbs4 = 0:dw_prbs4:w_max-dw_prbs4;
w_prbs1 = 0:dw_prbs1:w_max-dw_prbs1;

if fig
figure(4)
subplot(2,1,1)
semilogx(w_chirp, u_chirp_ampl(1:floor(N_chirp/2)));
hold on;
plot(w_prbs4, u_prbs4_ampl(1:floor(N_prbs4/2)));
hold on;
semilogx(w_prbs1, u_prbs1_ampl(1:floor(N_prbs1/2)));
grid on;
title('Amplitudengang');
ylabel('Amplitude [dB]');
xlabel('Frequenz [rad/s]');
legend('Chirp-Signal', 'PRBS, Pp = 4', 'PRBS, Pp = 1','Location','southwest');

subplot(2,1,2)
semilogx(w_chirp, u_chirp_phase(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, u_prbs4_phase(1:floor(N_prbs4/2)));
hold on;
semilogx(w_prbs1, u_prbs1_phase(1:floor(N_prbs1/2)));
hold off;
grid on;
title('Frequenzgang');
ylabel('Phase [°]');
xlabel('Frequenz [rad/s]');
end


%% 1.3 ETFE

%% 1. Simulation

% Parameters
parEMS.T_a = Ta;
parEMS.s0 = 0;
parEMS.w0 = 0;
simtime = Ta*(N-1);

% Simulation Chirp

parEMS.t_in = t_chirp';
parEMS.u_in = u_chirp';

sim('Einmassenschwinger_ETFE', simtime);

y_chirp_mr = sim_mit_rauschen;
y_chirp_kr = sim_kein_rauschen;

% Simulation PRBS

parEMS.t_in = t_prbs4';
parEMS.u_in = u_prbs4';

sim('Einmassenschwinger_ETFE', simtime);

y_prbs4_mr = sim_mit_rauschen;
y_prbs4_kr = sim_kein_rauschen;

%% 2. Schätzung der Übertragungsfunktion Chrip

% FFT
y_chirp_kr_fft = fft(y_chirp_kr.signals.values,N_chirp);
y_chirp_mr_fft = fft(y_chirp_mr.signals.values,N_chirp);

G_chirp_kr_ampl = 20*log10(abs(y_chirp_kr_fft)./abs(u_chirp_fft));
G_chirp_kr_phase = unwrap(angle(y_chirp_kr_fft) - angle(u_chirp_fft))*180/pi;
G_chirp_mr_ampl = 20*log10(abs(y_chirp_mr_fft)./abs(u_chirp_fft));
G_chirp_mr_phase = unwrap(angle(y_chirp_mr_fft) - angle(u_chirp_fft))*180/pi;

% Schätzung der Übertragungsfunktion PRBS

% FFT
y_prbs4_kr_fft = fft(y_prbs4_kr.signals.values,N_prbs4);
y_prbs4_mr_fft = fft(y_prbs4_mr.signals.values,N_prbs4);

G_prbs4_kr_ampl = 20*log10(abs(y_prbs4_kr_fft)./abs(u_prbs4_fft));
G_prbs4_kr_phase = unwrap(angle(y_prbs4_kr_fft) - angle(u_prbs4_fft))*180/pi;
G_prbs4_mr_ampl = 20*log10(abs(y_prbs4_mr_fft)./abs(u_prbs4_fft));
G_prbs4_mr_phase = unwrap(angle(y_prbs4_mr_fft) - angle(u_prbs4_fft))*180/pi;

% Zeitdiskretes Modell aus Aufgabe 1.1:
m = 0.5;   % [kg]
d = 0.7;   % [Ns/m]
c = 100.0; % [N/m]
Ta = 25e-3;   % [s]

% System zeitkontinuierlich:
s = tf('s');
Gs = 1/(m*s^2 + d*s + c);

% System zeitdiskret:
Gz = c2d(Gs, Ta, 'zoh');

[Gz_mag,Gz_phase, Gz_w] = bode(Gz,{2*pi/(N*Ta),w_max});
Gz_ampl = 20*log10(squeeze(Gz_mag(1,1,:)));
Gz_phase = squeeze(Gz_phase(1,1,:));


figure(5)
title('Ohne Rauschen');
subplot(2,1,1)
semilogx(w_chirp, G_chirp_kr_ampl(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, G_prbs4_kr_ampl(1:floor(N_prbs4/2)));
hold on;
semilogx(Gz_w, Gz_ampl);
grid on;
title('Amplitudengang ohne Rauschen');
ylabel('Amplitude [dB]');
xlabel('Frequenz [rad/s]');
legend('Chirp-Signal', 'PRBS, Pp = 4','Modell','Location','northwest');

subplot(2,1,2)
semilogx(w_chirp, G_chirp_kr_phase(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, G_prbs4_kr_phase(1:floor(N_prbs4/2)));
hold on;
semilogx(Gz_w, Gz_phase);
hold off;
grid on;
title('Frequenzgang ohne Rauschen');
ylabel('Phase [°]');
xlabel('Frequenz [rad/s]');


%% 3. Schätzungen mit Messrauschen

figure(6)
subplot(2,1,1)
semilogx(w_chirp, G_chirp_mr_ampl(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, G_prbs4_mr_ampl(1:floor(N_prbs4/2)));
hold on;
semilogx(Gz_w, Gz_ampl);
grid on;
title('Amplitudengang mit Rauschen');
ylabel('Amplitude [dB]');
xlabel('Frequenz [rad/s]');
legend('Chirp-Signal', 'PRBS, Pp = 4','Modell','Location','northwest');

subplot(2,1,2)
semilogx(w_chirp, G_chirp_mr_phase(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, G_prbs4_mr_phase(1:floor(N_prbs4/2)));
hold on;
semilogx(Gz_w, Gz_phase);
hold off;
grid on;
title('Frequenzgang mit Rauschen');
ylabel('Phase [°]');
xlabel('Frequenz [rad/s]');
