%% Aufgabe 1.1 Mathematisches Modell
close all;
clear all;
clc;

%Variablen
Ta=0.025;       %Abtastzeit
m=0.5;          %Masse
d=0.7;          %Dämpfung
c=100;          %Federsteifigkeit
N=4096;         %Messwerte

%Übertragungsfunktion
s = tf('s');    %Definition s-Variable
%y''*m + y'*d +y*c=F
%Y*s^2*m -m*(s*0+sm)+ Y*s*d +Y*d*sm +c=u mit Anfangsbedingungen
G.s = 1/(m*s^2 + d*s + c);   %Übertragungsfunktion
G.d.t25=c2d(G.s,Ta);      %Diskrete Überstragungsfunktion
bode(G.s,G.d.t25);
legend('G(s)','G(z)');

%Resonanzfrequenz 
G.w.w0=sqrt(c/m);
G.f.f0=G.w.w0/(2*pi);

%Frequenzauflösung 
G.w.max=pi/Ta;
G.w.min=2*pi/(N*Ta);

G.f.max = G.w.max/(2*pi);
G.f.min = G.w.min/(2*pi);

G.f.delta=1/(Ta*N);
disp('Aufgabe 1.1 ausgeführt');

%% Aufgabe 1.2

%Variablen
G.w.start=2*pi();
G.w.ende=20*pi();
U=10;

%Struct
chirp=struct;
prbs=struct;

%Signale
[chirp.t,chirp.u]=chirpsignal(U,G.w.start,G.w.ende,N,Ta);
[prbs.t1,prbs.u1]=PRBS(U,1,Ta);
[prbs.t,prbs.u]=PRBS(U,4,Ta);

%FFT
chirp.fft_d=fft(chirp.u,length(chirp.u));
prbs.fft_d=fft(prbs.u,length(prbs.u));
prbs.fft1_d=fft(prbs.u1,length(prbs.u1));

%Doppelte Werte entfernen
chirp.fft=chirp.fft_d(1:floor(N/2));
prbs.fft=prbs.fft_d(1:floor(N/2));
prbs.fft1=prbs.fft1_d(1:floor(length(prbs.u1)/2));

%Omega skallierung
chirp.w=2*pi()*(1:floor(N/2))/(N*Ta);
prbs.w=2*pi()*(1:floor(N/2))/(N*Ta);
prbs.w1=4*2*pi()*(1:floor(length(prbs.u1)/2))/(length(prbs.u1)*Ta);

figure('Name','Frequenzanalyse Chirpsignal');
subplot(2,1,1);
semilogx(chirp.w,20*log10(abs(chirp.fft)));
hold on;
semilogx(prbs.w1,20*log10(abs(prbs.fft1)));
semilogx(prbs.w,20*log10(abs(prbs.fft)));
legend('Chirp','PRBS1','PRBS4');
hold off;
subplot(2,1,2);
semilogx(chirp.w,unwrap(angle(chirp.fft)*180/pi()));
hold on;
semilogx(prbs.w1,unwrap(angle(prbs.fft1)*180/pi()));
semilogx(prbs.w,unwrap(angle(prbs.fft)*180/pi()));
legend('Chirp','PRBS1','PRBS4');
hold off;

%PRBS Signal mit Pp=1
figure('Name','Signale');
subplot(3,1,1);
plot(chirp.t,chirp.u);
title('Chirp');
xlabel('sekunden');
subplot(3,1,2);
prbs.u1_long=zeros(length(prbs.t),1);
prbs.u1_long(1:length(prbs.t1))=prbs.u1;
plot(prbs.t,prbs.u1_long);
title('PRBS1');
xlabel('sekunden');
subplot(3,1,3);
plot(prbs.t,prbs.u);
title('PRBS4');
xlabel('sekunden');



%% Aufgabe 1.3
%PRBS Simulation
[prbs.t,prbs.u]=PRBS(U,4,Ta);
parEMS=struct;
parEMS.t_in=prbs.t';
parEMS.u_in=prbs.u';
parEMS.T_a=Ta;
parEMS.s0=0;
parEMS.w0=0;
sim('Einmassenschwinger_ETFE',parEMS.t_in);

%Speichern der Ergebnisse
prbs.skr=sim_kein_rauschen;
prbs.smr=sim_mit_rauschen;

%Berechnen des Frequenzganges
G_etfe=fft(prbs.skr.signals.values)./fft(prbs.u);
%Entfernen der doppelten Einträge
prbs.G.etfe.o=G_etfe(1:floor(N/2));
G_etfe=fft(prbs.smr.signals.values)./fft(prbs.u);
prbs.G.etfe.m=G_etfe(1:floor(N/2));

%G(s) response
jw=1j*prbs.w;
G.s_w = 1./(m*jw.^2 + d.*jw + c);

%Darstellen der Ergebnisse von PRBS ohne Messrauschen
figure('Name','PRBS Signal');
subplot(2,1,1);
semilogx(prbs.w,20*log10(abs(prbs.G.etfe.o)));
hold on;
semilogx(prbs.w,20*log10(abs(G.s_w)));
semilogx(prbs.w,20*log10(abs(prbs.G.etfe.m)));
legend('ETFE ohne Rauschen','G(s)','ETFE mit Rauschen');
ylabel('Amplide');
xlabel('Frequenz');

subplot(2,1,2);
semilogx(prbs.w,unwrap(angle(prbs.G.etfe.o)*180/pi())-180);
hold on;
semilogx(prbs.w,unwrap(angle(G.s_w)*180/pi()));
semilogx(prbs.w,unwrap(angle(prbs.G.etfe.m)*180/pi())-180);
legend('ETFE ohne Rauschen','G(s)','ETFE mit Rauschen');
ylabel('Phase');
xlabel('Frequenz');

%Chirp Simulation

[chirp.t,chirp.u]=chirpsignal(U,G.w.start,G.w.ende,N,Ta);
clear parEMS;
parEMS=struct;
parEMS.t_in=chirp.t;
parEMS.u_in=chirp.u;
parEMS.T_a=Ta;
parEMS.s0=0;
parEMS.w0=0;
sim('Einmassenschwinger_ETFE',parEMS.t_in);

%Speichern der Ergebnisse
chirp.skr=sim_kein_rauschen;
chirp.smr=sim_mit_rauschen;

%Berechnen des Frequenzganges
G_etfe=fft(chirp.skr.signals.values)./fft(chirp.u)';
%Entfernen der doppelten Einträge
chirp.G.etfe.o=G_etfe(1:floor(N/2));
G_etfe=fft(chirp.smr.signals.values)./fft(chirp.u)';
chirp.G.etfe.m=G_etfe(1:floor(N/2));

chirp.w=2*pi()*(1:floor(N/2))/(N*Ta);

%Darstellen der Ergebnisse von PRBS ohne Messrauschen
figure('Name','Chirp Signal');
subplot(2,1,1);
semilogx(chirp.w,20*log10(abs(chirp.G.etfe.o)));
hold on;
jw=1j*chirp.w;
G.s_w = 1./(m*jw.^2 + d.*jw + c);
semilogx(chirp.w,20*log10(abs(G.s_w)));
semilogx(chirp.w,20*log10(abs(chirp.G.etfe.m)));
legend('ETFE ohne Rauschen','G(s)','ETFE mit Rauschen');
ylabel('Amplide');
xlabel('Frequenz');
subplot(2,1,2);
semilogx(chirp.w,unwrap(angle(chirp.G.etfe.o)*180/pi()));
hold on;
semilogx(chirp.w,unwrap(angle(G.s_w)*180/pi()));
semilogx(chirp.w,unwrap(angle(chirp.G.etfe.m)*180/pi())-180);
legend('ETFE ohne Rauschen','G(s)','ETFE mit Rauschen');
ylabel('Phase');
xlabel('Frequenz');

%% Aufgabe 1.4

Ta_n=0.001:0.025:0.05;  %verschiedene Abtastzeiten

for i=1:length(Ta_n)
    clear z;
    z=tf('z',Ta_n(i));              %z als DISKRETE übertragungsfunktion
    G.d.Ta_n=c2d(G.s,Ta_n(i));      %Diskretisieren
    G.d.euler = Ta_n(i) ^ 2 / (m * z ^ 2 + (Ta_n(i) * d - 2 * m) * z + c * Ta_n(i) ^ 2 - Ta_n(i) * d + m);
    G.d.trapez = Ta_n(i)^2*(z^2 +2*z+1)/((c*Ta_n(i) ^ 2 + 2 * Ta_n(i) * d + 4 * m) * z ^ 2 + (2 * c * Ta_n(i) ^ 2 - 8 * m) * z + c * Ta_n(i) ^ 2 - 2 * Ta_n(i) * d + 4 * m);                                        
    figure('Name',['Abtastzeit ' num2str(Ta_n(i)*1000) ' ms']);
    pzmap(G.d.Ta_n, 'b');
    hold on;
    pzmap(G.d.euler, 'm');
    hold on;
    pzmap(G.d.trapez, 'y');
    legend('Genue G','Euler G', 'Trapez G');
    figure('Name',['Abtastzeit ' num2str(Ta_n(i)*1000) ' ms']);
    subplot(3,1,1);
    step(G.d.Ta_n);
    legend('Genue G');
    subplot(3,1,2);
    step(G.d.trapez);   
    legend('Trapez G');
    subplot(3,1,3);
    step(G.d.euler);
    legend('Euler G');
end

%% Aufgabe 1.5

G.d.gt.trapez_neu =z^2*Ta^2/(c*Ta^2*(z+1)^2+(2*d*z^2-2*d)*Ta+4*m*(z-1)^2);

G.d.gt.a1= (2*c*Ta^2-8*m)/(c*Ta+2*d*Ta+4*m);
G.d.gt.a2= (c*Ta^2-2*d*Ta+4*m)/(c*Ta+2*d*Ta+4*m);
G.d.gt.b0= Ta^2/(c*Ta^2+2*d*Ta+4*m);


%sk=[-y_1, -y_2, uk]
G.d.gt.p=[G.d.gt.a1; G.d.gt.a2; G.d.gt.b0];

G.d.gt.m = -Ta^2*(G.d.gt.a1-G.d.gt.a2-1)/(16*G.d.gt.b0);        %Masse
G.d.gt.d = -(G.d.gt.a2-1)*Ta /(4*G.d.gt.b0);                    %Dämpfung
G.d.gt.c = (G.d.gt.a1+G.d.gt.a2+1)/(4*G.d.gt.b0);               %Federkonstante

%% Aufgabe 1.6 Diskrete Parameteridentifikation
dparm=struct;
dparm.t_sim=100;              %Simulationszeit in Sekunden
dparm.shift=10;               %Ersten Werte werden verworfen
dparm.Pp=4;                   %Überabtastung

[dparm.t,dparm.u]=PRBS(10,dparm.Pp,Ta);
%Startwerte
parEMS.t_in=dparm.t';
parEMS.u_in=dparm.u';
parEMS.T_a=Ta;
parEMS.s0=0;
parEMS.w0=0;

sim('Einmassenschwinger_PARID.slx',dparm.t_sim);

%Eingangsvariablen
dparm.uk.uk=sim_eingang.signals.values(dparm.shift:end);
dparm.uk.uk_1=sim_eingang.signals.values(dparm.shift-1:end-1);
dparm.uk.uk_2=sim_eingang.signals.values(dparm.shift-2:end-2);
dparm.uk.uk_new=dparm.uk.uk+2*dparm.uk.uk_1+dparm.uk.uk_2;

%Ausgangsvariablen ohne Rauschen
dparm.o.yk =sim_kein_rauschen.signals.values(dparm.shift:end);
dparm.o.yk_1=sim_kein_rauschen.signals.values(dparm.shift-1:end-1);
dparm.o.yk_2=sim_kein_rauschen.signals.values(dparm.shift-2:end-2);

%Ausgangsvariablen mit Rauschen
dparm.m.yk =sim_mit_rauschen.signals.values(dparm.shift:end);
dparm.m.yk_1=sim_mit_rauschen.signals.values(dparm.shift-1:end-1);
dparm.m.yk_2=sim_mit_rauschen.signals.values(dparm.shift-2:end-2);

%Datenvektor transponiert
dparm.o.St=[-dparm.o.yk_1, -dparm.o.yk_2, dparm.uk.uk_new];
dparm.m.St=[-dparm.m.yk_1, -dparm.m.yk_2, dparm.uk.uk_new];

%Punkt 2 Parametervektor
dparm.o.p=(dparm.o.St'*dparm.o.St)\dparm.o.St'*dparm.o.yk;
dparm.m.p=(dparm.m.St'*dparm.m.St)\dparm.m.St'*dparm.m.yk;

%Werte errechnen
dparm.o.values.m=-Ta^2*(dparm.o.p(1)-dparm.o.p(2)-1)/(16*dparm.o.p(3));
dparm.o.values.c=(dparm.o.p(1)+dparm.o.p(2)+1)/(4*dparm.o.p(3));
dparm.o.values.d=-(dparm.o.p(2)-1)*Ta /(4*dparm.o.p(3));

dparm.m.values.m=-Ta^2*(dparm.m.p(1)-dparm.m.p(2)-1)/(16*dparm.m.p(3));
dparm.m.values.c=(dparm.m.p(1)+dparm.m.p(2)+1)/(4*dparm.m.p(3));
dparm.m.values.d=-(dparm.m.p(2)-1)*Ta /(4*dparm.m.p(3));

%Ergebnisse für Tsim =100sec
%Pp=4
%       ohne        mit
%m      0.496       0.4901
%c      99.86       98.94
%d      0.603       0.67

%Punkt 3
%Ergebnisse für Tsim =100sec
%Pp=6
%       ohne        mit
%m      0.497       0.4928
%c      99.9       99.31
%d      0.65       0.7152


% Punkt 4
%Tsim=50sec
%Pp=4
%       ohne        mit
%m      0.4957      0.4883
%c      99.87       98.83
%d      0.574       0.6816

%% Aufgabe 1.7 Zustandsvariablenfilter

zvf=struct;
%Punkt 1
%y''*m + y'*d +y*c=F

[zvf.t,zvf.u]=PRBS(10,4,Ta);
%Startwerte
parEMS.t_in=zvf.t';
parEMS.u_in=zvf.u';
parEMS.T_a=Ta;
parEMS.s0=0;
parEMS.w0=0;

%Punkt 3
zvf.Tf = 0.1;           %Filterzeitkonstante
zvf.Ta = 0.001;         %Abtastzeit

%Punkt 2 Filterkoeffizienten
zvf.f0 = 1 / zvf.Tf^3;
zvf.f1 = 3 / zvf.Tf^2;
zvf.f2 = 3 / zvf.Tf;

zvf.A = [0, 1, 0; 0, 0, 1; -zvf.f0, -zvf.f1, -zvf.f2];
zvf.B = [0, 0, 1]';
zvf.C_u = [zvf.f0, 0, 0];
zvf.C_y = zvf.f0 * eye(3);
zvf.D = 0;

zvf.y_c = ss(zvf.A, zvf.B, zvf.C_y, zvf.D);
zvf.y_d = c2d(zvf.y_c, zvf.Ta,'zoh');

zvf.u_c = ss(zvf.A, zvf.B, zvf.C_u, zvf.D);
zvf.u_d = c2d(zvf.u_c, zvf.Ta,'zoh');

ZVF_y=zvf.y_d;
ZVF_u=zvf.u_d;

%% Aufgabe 1.8 Kontinuierliche Parameteridentifikation

zvf.T_sim=40;       %Simulationszeit
sim('Einmassenschwinger_PARID.slx', zvf.T_sim);

%Variablen
zvf.o.y0=sim_ZVF_y.signals.values(:,3);
zvf.o.y1=sim_ZVF_y.signals.values(:,2);
zvf.o.y2=sim_ZVF_y.signals.values(:,1);

zvf.m.y0=sim_ZVF_y_rausch.signals.values(:,3);
zvf.m.y1=sim_ZVF_y_rausch.signals.values(:,2);
zvf.m.y2=sim_ZVF_y_rausch.signals.values(:,1);

zvf.u0=sim_ZVF_u.signals.values;

%Datenvektor Transponiert
zvf.o.St=[-zvf.o.y1,-zvf.o.y2,zvf.u0];
zvf.m.St=[-zvf.m.y1,-zvf.m.y2,zvf.u0];

%Parametervektor
zvf.o.p=(zvf.o.St'*zvf.o.St)\zvf.o.St'*zvf.o.y0;
zvf.m.p=(zvf.m.St'*zvf.m.St)\zvf.m.St'*zvf.m.y0;

%Werte errechnen
zvf.o.values.m=1/zvf.o.p(3);
zvf.o.values.c=zvf.o.p(2)*zvf.o.values.m;
zvf.o.values.d=zvf.o.p(1)*zvf.o.values.m;

zvf.m.values.m=1/zvf.m.p(3);
zvf.m.values.c=zvf.m.p(2)*zvf.m.values.m;
zvf.m.values.d=zvf.m.p(1)*zvf.m.values.m;

%Simulationsergebnisse
%T_sim=20 sec
%       ohne        mit
%m      0.5004      0.5007
%c      100.0002	100.0479
%d      0.7112      0.7102

%T_sim=40 sec
%       ohne        mit
%m      0.5004      0.5006
%c      100.0003	100.0469
%d      0.7077      0.7074

%% Aufgabe 1.9
rls=struct;
parRLS=struct;
parRLS.T_a=Ta;
rls.T_sim=100;      %Simulationszeit

rls.alpha0=1;       %Gewichtskoeffizenten
rls.q=0.995;        %Gedächnisfaktor

RLSq=rls.q;
RLSalpha0=rls.alpha0;
sim('Einmassenschwinger_REK.slx', rls.T_sim);
