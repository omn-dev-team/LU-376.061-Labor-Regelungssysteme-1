function [ u, t ] = Chirp( U, ws, we, N, Ta )
% Chirp-Signal mit trapezf�rmiger Fensterung wird nach den Formeln 1.1 und
% 1.2  erstellt (Skript Labor RS1, S.5), welches mittelwertfrei ist.
%
% Eingabe-Parameter:
% U  ... Amplitude (entspricht U_dach im Skript)
% ws ... Startfrequenz
% we ... Endfrequenz
% N  ... Anzahl der Messpunkte
% Ta ... Abtastzeit [s]
%
% Ausgabeparameter:
% u  ... Signalvektor
% t  ... Zeitvektor

k = (0:N-1)';

sat1 = 10*k/N;
sat2 = 10*(N-k)/N;

sat1(sat1 > 1) = 1;
sat1(sat1 < -1) = -1;
sat2(sat2 > 1) = 1;
sat2(sat2 < -1) = -1;

r = U*sat1.*sat2;

u = r.*sin(ws*k*Ta + ((we-ws)/(N*Ta))*0.5*(k*Ta).^2);

% Korrektur f�r mittelwertfreies Signal
U0 = mean(u);
u = u - U0;

t = k*Ta;
end