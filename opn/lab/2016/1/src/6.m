%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.6: Diskrete Parameteridentifikation
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.6.1 Simulation

% Einstellparameter:
Tsim = 20;

% Parameter PRBS:
Ta = 25e-3;
N = Tsim/Ta;
U = 10;
Pp = 5;

[u_prbs4, t_prbs4] = PRBS(U, Pp, Ta);

parEMS.t_in = t_prbs4';
parEMS.u_in = u_prbs4';
parEMS.T_a = Ta;
parEMS.s0 = 0;
parEMS.w0 = 0;

sim('Einmassenschwinger_PARID_kZVF', Tsim);

%% 1.6.2 Least Squares Sch�tzer:

% Abtastschritt, ab dem die Matrix S gebildet wird start >= 2 
start = 10;


% Ausg�nge y(k), y(k-1), y(k-2)
yk = sim_kein_rauschen.signals.values(start:end);
yk_1 = sim_kein_rauschen.signals.values(start-1:end-1);
yk_2 = sim_kein_rauschen.signals.values(start-2:end-2);

% Eingang u_quer = u(k) + 2*u(k-1) + u(k-2)
u = sim_eingang.signals.values;
u_quer = u(start:end) + 2*u(start-1:end-1) + u(start-2:end-2);

% Datenmatrix und Parametersch�tzung
S = [-yk_1, -yk_2, u_quer];
p_est = (S'*S)\S'*yk;

% Parameter nach 1.5 (Maple)
par.m = (-1/16)*Ta^2*(p_est(1)-p_est(2)-1)/p_est(3);
par.d = (-1/4)*Ta*(p_est(2)-1)/p_est(3);
par.c = (1/4)*(p_est(1) + p_est(2) +1)/p_est(3);
par.t = Tsim;
par.S_cond = cond(S'*S);

par

% Ausg�nge mit Rauschen yR(k), yR(k-1), yR(k-2)
yRk = sim_mit_rauschen.signals.values(start:end);
yRk_1 = sim_mit_rauschen.signals.values(start-1:end-1);
yRk_2 = sim_mit_rauschen.signals.values(start-2:end-2);

% Datenmatrix und Parametersch�tzung
SR = [-yRk_1, -yRk_2, u_quer];
pR_est = (SR'*SR)\SR'*yRk;

% Parameter nach 1.5 (Maple)
parR.m = (-1/16)*Ta^2*(pR_est(1)-pR_est(2)-1)/pR_est(3);
parR.d = (-1/4)*Ta*(pR_est(2)-1)/pR_est(3);
parR.c = (1/4)*(pR_est(1) + pR_est(2) +1)/pR_est(3);
parR.t = Tsim;
parR.S_cond = cond(SR'*SR);

parR


%% 1.6.3

% F�r eine h�here �berabtastung verl�ngert sich die Dauer jedes einzelnen
% PRBS-Pulses, f�r die das Signal konstant bleibt. Deshalb hat das System
% mehr Zeit zum Einschwingen und die Sch�tzung der Parameter verbessert
% sich. Die Gr��te Auswirkung ergibt sich f�r die Sch�tzung des
% D�mpfungsfaktors.

% F�r eine l�ngere Simulationsdauer k�nnen mehr Messdaten zur Sch�tzung der
% Parameter gesammelt werden -> Sch�tzung wird besser.

% Die Kondition der Matrix nimmt mit steigender Simulationsdauer ab. Die
% Kondition hat ein Minimum f�r eine �berabtastung von ca. 5.

%% 1.6.4

% F�r eine l�ngere Simulationsdauer stehen mehr Daten zur Verf�gung und es
% sinkt der Einfluss des Rauschens auf die Sch�tzung der Parameter, da die
% Fehler gemittelt werden.

%% 1.6.5

% Verrauschte Signale geben kaum Aufschluss �ber die Systemordnung. Durch
% Annahme einer h�heren Systemordnung als tats�chlich vorhanden werden
% durch die Identifikationsaufgabe Parameter gesch�tzt, die im System nicht
% vorhanden sind.

