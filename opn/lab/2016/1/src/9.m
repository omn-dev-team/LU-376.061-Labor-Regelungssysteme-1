%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.9: Rekursive Least-Squares Idetifikation
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameter:
Ta = 25e-3;
Tsim = 100;
N = Tsim/Ta;
U = 10;
Pp = 40;

[u_prbs, t_prbs] = PRBS(U, Pp, Ta);

% Simulation:

parEMS.t_in = t_prbs';
parEMS.u_in = u_prbs';
parEMS.T_a = Ta;
parEMS.s0 = 0;
parEMS.w0 = 0;

% Anfangswerte:
parRLS.T_a = Ta;
% RLSalpha0 = 100000;
RLSalpha0 = 1;
RLSq = 0.99;

sim('Einmassenschwinger_REK', Tsim);


% 1.9.2

% F�r hohe Werte von alpha (Fehlerkovarianzmatrix ist gro�) wird dem
% Startwert weniger vertraut und die Konvergenzgeschwindigkeit ist gr��er
% (Zeitdauer ist geringer), als f�r niedrige Werte von alpha.
% P = alpha*E

% 1.9.3

% F�r einen Ged�chtnisfaktor von q = 0.995 kann die Sch�tzung der Rampe
% folgen. Ein kleiner konstanter Offset bleibt. Verringert man den
% Ged�chtnisfaktor auf \zB q = 0.9 wird der Offset geringer, jedoch werden
% einzelne Ausrei�er deutlich gr��er.

% 1.9.4

% Ein h�herer Ged�chtnisfaktor sorgt f�r eine bessere Rauschunterdr�ckung,
% da die vorherigen Werte st�rker gewichtet werden. Deshalb hat eine
% aktuell auftretende St�rung weniger Einfluss auf die Parametersch�tzung.
