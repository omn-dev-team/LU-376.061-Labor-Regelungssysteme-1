%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.1: Modell des Einmassenschwingers
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variablen:
% Masse m
% Dämpfung d
% Federsteifigkeit c
% Position sm                       = Ausgang y
% Geschwindigkeit wm = d/dt sm
% Kraft F                           = Stelleingang u
% Übertragungsfunktion Gs = y/u
% Abtastzeit Ta

% Werte:
m = 0.5;   % [kg]
d = 0.7;   % [Ns/m]
c = 100.0; % [N/m]
Ta = 25e-3;   % [s]

% System zeitkontinuierlich:
s = tf('s');
Gs = 1/(m*s^2 + d*s + c);
w0 = sqrt(c/m);
f0 = w0/(2*pi);

% System zeitdiskret:
Gz = c2d(Gs, Ta, 'zoh');

% Bodeplot:
figure(1)
bode(Gs);
grid on;