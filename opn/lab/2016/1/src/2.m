%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.2: Testsignale
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;

% Parameter f�r Chirp- und PRBS-Signal:
N = 4096;
ws = 2*pi;    % [rad/s]
we = 10*2*pi; % [rad/s]
U = 10;
Ta = 25e-3;   % [s]

% Chirp Signal:
[ u_chirp, t_chirp ] = Chirp( U, ws, we, N, Ta );

% PRBS-Signal f�r Pp = 4:
[ u_prbs4, t_prbs4 ] = PRBS( U, 5, Ta );

% PRBS-Signal f�r Pp = 1:
[ u_prbs1, t_prbs1 ] = PRBS( U, 1, Ta );

% Darstellung der Zeitverl�ufe
figure(1)
plot(t_chirp, u_chirp);
title('Zeitverlauf Chirp-Signal');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;

figure(2)
plot(t_prbs4, u_prbs4);
title('Zeitverlauf PRBS-Signal mit Pp = 4');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;

figure(3)
plot(t_prbs1, u_prbs1);
title('Zeitverlauf PRBS-Signal mit Pp = 1');
xlabel('Zeit t [s]');
ylabel('Amplitude U');
grid on;

% Vergleich des Amplitudenspektrums
% FFT normiert auf Signall�nge
N_chirp = length(u_chirp);
N_prbs4 = length(u_prbs4);
N_prbs1 = length(u_prbs1);

u_chirp_fft = fft(u_chirp,N_chirp);
u_prbs4_fft = fft(u_prbs4,N_prbs4);
u_prbs1_fft = fft(u_prbs1,N_prbs1);

% Amplitude in dB und Phase in � der Signale
u_chirp_ampl = 20*log10(abs(u_chirp_fft));
u_chirp_phase = unwrap(angle(u_chirp_fft))*(180/pi);
u_prbs4_ampl = 20*log10(abs(u_prbs4_fft));
u_prbs4_phase = unwrap(angle(u_prbs4_fft))*(180/pi);
u_prbs1_ampl = 20*log10(abs(u_prbs1_fft));
u_prbs1_phase = unwrap(angle(u_prbs1_fft))*(180/pi);

% Minimale Frequenz
dw_chirp = 2*pi/(N_chirp*Ta);
dw_prbs4 = 2*pi/(N_prbs4*Ta);
dw_prbs1 = 2*pi/(N_prbs1*Ta);

% Maximale Frequenz
w_max = pi/Ta;

% physikalisch sinnvoller Bereich
w_chirp = 0:dw_chirp:w_max-dw_chirp;
w_prbs4 = 0:dw_prbs4:w_max-dw_prbs4;
w_prbs1 = 0:dw_prbs1:w_max-dw_prbs1;

figure(4)
subplot(2,1,1)
semilogx(w_chirp, u_chirp_ampl(1:floor(N_chirp/2)));
hold on;
plot(w_prbs4, u_prbs4_ampl(1:floor(N_prbs4/2)));
hold on;
semilogx(w_prbs1, u_prbs1_ampl(1:floor(N_prbs1/2)));
grid on;
title('Amplitudengang');
ylabel('Amplitude [dB]');
xlabel('Frequenz [rad/s]');
legend('Chirp-Signal', 'PRBS, Pp = 4', 'PRBS, Pp = 1','Location','southwest');

subplot(2,1,2)
semilogx(w_chirp, u_chirp_phase(1:floor(N_chirp/2)));
hold on;
semilogx(w_prbs4, u_prbs4_phase(1:floor(N_prbs4/2)));
hold on;
semilogx(w_prbs1, u_prbs1_phase(1:floor(N_prbs1/2)));
hold off;
grid on;
title('Frequenzgang');
ylabel('Phase [�]');
xlabel('Frequenz [rad/s]');
