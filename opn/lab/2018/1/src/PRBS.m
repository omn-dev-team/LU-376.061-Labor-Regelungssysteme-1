function [t,u] = PRBS(U,Pp,Ta)
%PRBS Pseudo Random Binary Signal

%Input
%U Amplitude
%Pp ‹berabtastung
%Ta Abtastzeit

%Output
%t Zeit
%u Werte

%Variablen
Op=10;
a=[0 0 0 0 0 0 0 0 0 1];
N=(2^(Op)-1);

%Initalisieren
pk=zeros(1,Op);
p=zeros(1,N);
p(1:Op)=a;

%Berechnen des Signals
for k=Op+1:N
 pk=p(k-Op:k-1);
p(k)=mod(pk(4)+pk(1),2);
end
%Zusammensetzen
u=(2*U*p)'-mean(2*U*p);

%‹berabtasten
u = reshape(repmat(u,1,Pp)',size(u,1)*Pp,[]);
t = ((0:N*Pp-1).*Ta)';
end

