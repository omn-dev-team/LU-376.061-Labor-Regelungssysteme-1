function [t,u]=chirpsignal(U,w_s,w_e,N,Ta)
% linear chirp-signal mit trapezfoermiger Fensterung
%Input
%U Amplitude
%w_s Startfrequenz
%w_e Endfrequenz
%N Abtastpunkte
%Ta Abtastzeit

%Output
%t Zeitvektor
%u Signalvektor

k = 0:N-1;

r_k = U.*sat((10.*k)/N).*sat((10*(N-k))/N);
phi_k = w_s*k*Ta + ((w_e-w_s)/(N*Ta))*(((k*Ta).^2)/2);

Uo = -mean(r_k.*sin(phi_k));

u = Uo+(r_k.*sin(phi_k));
t = 0:Ta:(N-1)*Ta;
%Graphisch darstellen (nur zur Probe)
%plot(t,u)
  
end

function [ x ] = sat( x )

i=find(x>=1);
x(i)=1;

j=find(x<=-1);
x(j)=-1;


end
  
