%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.4: Numerische Diskretisierung
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;

% % Zeitdiskretes Modell aus Aufgabe 1.1 bzw. 1.3
% m = 0.5;      % [kg]
% d = 0.7;      % [Ns/m]
% c = 100.0;    % [N/m]
% Ta = 25e-3;   % [s]
% 
% % System zeitkontinuierlich
% s = tf('s');
% Gs = 1/(m*s^2 + d*s + c);
% 
% % System zeitdiskret
% z = tf('z');
% Gz = c2d(Gs, Ta, 'zoh');
% syms s z
% 
% % Maximale Frequenz
% w_max = pi/Ta;
% 
% % Diskretisierung mit Euler-Verfahren
% Gz_euler = subs(1/(m*s^2 + d*s + c),s,(z-1)/Ta);
% [Gz_euler_zaehler, Gz_euler_nenner] = numden(Gz_euler)
% Gz_euler = tf(sym2poly(Gz_euler_zaehler), sym2poly(Gz_euler_nenner), Ta) % coeffs
% 
% % Diskretisierung mit Heun-Verfahren
% Gz_heun = subs(1/(m*s^2 + d*s + c),s,2/Ta*(z-1)/(z+1));
% [Gz_heun_zaehler, Gz_heun_nenner] = numden(Gz_heun)
% Gz_heun = tf(sym2poly(Gz_heun_zaehler), sym2poly(Gz_heun_nenner), Ta)
% 
% figure(1)
% bode(Gs);
% title('Bode-Diagramm Zeitkontinuierliches System');
% grid on;
% 
% figure(2)
% subplot(1,3,1);
% bode(Gz, Gz_euler, Gz_heun);
% title('Bode-Diagramm Zeitdiskretes System');
% grid on;
% legend('G(z) original', 'G(z) Euler','G(z) Heun (Trapezregel)','Location','northeast');
% 
% % Pole und Nullstellen der z-Übertragungsfunktionen
% subplot(1,3,2);
% pzplot(Gz, Gz_euler, Gz_heun);
% title('Pol-Nullstellen-Diagramm der zeitdiskreten Übertragungsfunktionen');
% grid on;
% 
% % Sprungantworten
% dauer = 1.5;
% subplot(1,3,3);
% step(Gz, Gz_euler, Gz_heun, dauer);

%drawplots(25);
figure(2)
% Slider für Abtastzeit im Bereich 1-50ms
slider_Ta = uicontrol('Style', 'slider',...
    'Min',1,'Max',50,'Value',25,...
    'Position', [400 10 250 20],...
    'Callback', @sliderUsed);

slider_Ta_txt = uicontrol('Style','text',...
        'Position',[400 35 25 20],...
        'String','Abtastzeit in ms');
    
function sliderUsed(source,~)

Ta = source.Value / 1000;
%drawplots(Ta);

%end

%function drawplots(Ta)
        
% Zeitdiskretes Modell aus Aufgabe 1.1 bzw. 1.3
m = 0.5;      % [kg]
d = 0.7;      % [Ns/m]
c = 100.0;    % [N/m]
%Ta = 25e-3;   % [s]

% System zeitkontinuierlich
s = tf('s');
Gs = 1/(m*s^2 + d*s + c);

% System zeitdiskret
z = tf('z');
Gz = c2d(Gs, Ta, 'zoh');
syms s z

% Maximale Frequenz
w_max = pi/Ta;

% Diskretisierung mit Euler-Verfahren
Gz_euler = subs(1/(m*s^2 + d*s + c),s,(z-1)/Ta);
[Gz_euler_zaehler, Gz_euler_nenner] = numden(Gz_euler);
Gz_euler = tf(sym2poly(Gz_euler_zaehler), sym2poly(Gz_euler_nenner), Ta);
zeros_euler = zero(Gz_euler)
poles_euler = pole(Gz_euler)

% Diskretisierung mit Heun-Verfahren
Gz_heun = subs(1/(m*s^2 + d*s + c),s,2/Ta*(z-1)/(z+1));
[Gz_heun_zaehler, Gz_heun_nenner] = numden(Gz_heun);
Gz_heun = tf(sym2poly(Gz_heun_zaehler), sym2poly(Gz_heun_nenner), Ta)
zeros_heun = zero(Gz_heun)
poles_heun = pole(Gz_heun)

subplot(1,3,1);
bode(Gz, Gz_euler, Gz_heun);
title('Bode-Diagramm Zeitdiskretes System');
grid on;
legend('G(z) original', 'G(z) Euler','G(z) Heun (Trapezregel)','Location','northeast');

% Pole und Nullstellen der z-Übertragungsfunktionen
subplot(1,3,2);
pzplot(Gz, Gz_euler, Gz_heun);
title('Pol-Nullstellen-Diagramm der zeitdiskreten Übertragungsfunktionen');
grid on;

% Sprungantworten
dauer = 1.5;
subplot(1,3,3);
step(Gz, Gz_euler, Gz_heun, dauer);

end
