%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Labor Regelungssysteme 1, WS 16/17
%
% Aufgabe 1.7: Zustandsvariablenfilter
% Aufgabe 1.8: Kontinuierliche Parameteridentifikation
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Aufgabe 1.7: Zustandsvariablenfilter

% 1.7.3 Kontinuierliches Zustandsvariablenfilter:

% Filterzeitkonstante:
Tf = 0.1;
f0 = 1/Tf^3;
f1 = 3/Tf^2;
f2 = 3/Tf;                           

% Filtermatrizen:
A = diag([1,1],1);
A(3,:) = [-f0, -f1, -f2];
B = [0;0;1];
C = f0*eye(3);
D = [0; 0; 0];

ZVF = ss(A,B,C,D);

% 1.7.4 Diskretisierung des Filters:

% Zeitkonstante:
Ta_Zvf = 1e-3;

% Diskretes Filter:
ZVFz = c2d(ZVF, Ta_Zvf, 'zoh');

%% Aufgabe 1.8: Kontinuierliche Parameteridentifikation

% Simulationszeit:
Tsim = 40;

% Parameter PRBS:
Ta = 25e-3;
N = Tsim/Ta;
U = 10;
Pp = 4;

[u_prbs4, t_prbs4] = PRBS(U, Pp, Ta);

parEMS.t_in = t_prbs4';
parEMS.u_in = u_prbs4';
parEMS.T_a = Ta;
parEMS.s0 = 0;
parEMS.w0 = 0;

sim('Einmassenschwinger_PARID', Tsim);

% Least Squares Sch�tzer:

y = sim_ZVF_y.signals.values(:,1);
yp = sim_ZVF_y.signals.values(:,2);
ypp = sim_ZVF_y.signals.values(:,3);

S = [-y, -yp, sim_ZVF_u.signals.values(:,1)];
p = (S'*S)\S'*ypp;

% Parameter: p1 = a0 = c/m, p2 = a1 = d/m, p3 = b0 = 1/m
par.m = 1/p(3);
par.d = p(2)*par.m;
par.c = p(1)*par.m;
par.t = Tsim;

yR = sim_ZVF_y_rausch.signals.values(:,1);
yRp = sim_ZVF_y_rausch.signals.values(:,2);
yRpp = sim_ZVF_y_rausch.signals.values(:,3);

SR = [-yR, -yRp, sim_ZVF_u.signals.values(:,1)];
pR = (SR'*SR)\SR'*yRpp;

% Parameter: p1 = a0 = c/m, p2 = a1 = d/m, p3 = b0 = 1/m
parR.m = 1/pR(3);
parR.d = pR(2)*parR.m;
parR.c = pR(1)*parR.m;
parR.t = Tsim;

par
parR


%% 1.8.2

% Die Messdauer wirkt sich sehr stark auf das Ergbnis aus, wenn sie im
% Bereich der Zeitkonstante des Zustandsvariablenfilters ist, da diese dann
% nicht Einschwingen kann.

% F�r eine l�ngere Messdauer stehen mehr Daten zur Verf�gung und der
% Einfluss des Rauschens kann verringert werden.

% Bei einer l�ngeren Messdauer die Verbesserung der Sch�tzung des 
% D�mpfungsfaktors am gr��ten.

